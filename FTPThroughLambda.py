from __future__ import print_function
import boto3
import os
import sys
import ftplib
from botocore.exceptions import ClientError
from environs import Env
env = Env()

### CONFIGURATION ###

ip = env.string('ip')
username = env.string('user')
password = env.string('password')
remote_directory = env.string('remoteDirectory')
rm_local = env.bool('removeDownloadedObject', True)
rm_remote = env.bool('removeRemoteObject', True)
ftp_pasv = env.bool('ftpPasvMode', True)

s3_client = boto3.client('s3')

def FTPTransfer(local_file):
    ## NOTE: If we give download_path as the filename in storlines function,
    ## Lambda tries to create the file in the /tmp folder on the host 
    ## during the FTP transfer
    os.chdir("/tmp/")
    # print('Trying to connect to FTP at '+ip)

    try:
        ftp = ftplib.FTP(ip)
        ftp.login(username, password)
        # print(ftp.getwelcome())

        ftp.set_pasv(ftp_pasv)
        # print('FTP set to Passive Mode')

        # Change the working directory on the host
        if not remote_directory.strip():
            ftp.cwd(remote_directory)
            #print('FTP working directory in host changed to {}'.format(remote_directory))
            
            ## Initiate File transfer     

            ## DETAIL: Assume that the input file will always be a text file.
            ## In case if the file is other than text use the following commented statements
            # file = open(sourcekey,"rb")
            # ftp.storbinary('STOR ' + sourcekey, file)
            file = open(local_file,"r")

            local_basename = os.path.basename(local_file)
            ftp.storlines('STOR ' + local_basename, file)

            print('File '+local_basename+' has been uploaded to FTP server at '+ip+' successfully!')
    except Exception as e:
        raise Exception("Unexpected error in FTP function: %s" % e)

def lambda_handler(event, context):
    for record in event['Records']:
        sourcebucket = record['s3']['bucket']['name']
        sourcekey = record['s3']['object']['key'] 

        # Download the file to /tmp/ folder        
        download_path = '/tmp/'+os.path.basename(sourcekey)
        # print('Downloading s3://'+sourcebucket+'/'+sourcekey+' to '+download_path)
        try:
            s3_client.download_file(sourcebucket, sourcekey, download_path)
            print('Downloaded remote file from s3://'+sourcebucket+'/'+sourcekey+' to '+download_path)
        except ClientError as e:
            raise Exception("Unexpected error in S3 download: %s" % e)

        # Transfer the file to the FTP server
        FTPTransfer(download_path)

        # Remove downloaded file from Lambda storage
        ## Might be necessary in the case of function runtime reuse in high concurrency environments
        ## https://aws.amazon.com/blogs/compute/container-reuse-in-lambda/
        if rm_local is True:
            os.remove(download_path)
            print('Cleaned up container by deleting local file '+download_path)

        # Remove the file from the source S3 bucket
        if rm_remote is True:
            try:
                s3_client.delete_object(Bucket=sourcebucket, Key=sourcekey)
                print('Cleaned up S3 by deleting remote file {} from bucket {}'.format(sourcekey,sourcebucket))
            except ClientError as e:
                raise Exception("Unexpected error in S3 deletion: %s" % e)
        
        return
        

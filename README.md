**What does it do?**

This function will take files dropped into an S3 bucket and transfer them to an FTP server.

**Packaging**

To deploy to Lambda, the user must create an archive with the function, and it's requirements.

An example, doing this with `pyenv` and its `virtualenv` plugin, for Python 3.8:

```
git clone https://gitlab.com/erincerys/FTP_SFTP_LAMBDA.git
cd FTP_SFTP_LAMBDA
sudo pacman -S pyenv pyenv-virtualenv
pyenv install 3.8.6
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
pyenv virtualenv 3.8.6 lambda-packaging
. $(pyenv root)/versions/lambda-packaging/bin/activate
pip3 install -r requirements.txt
deactivate
packageDir=$(pyenv root)/versions/lambda-packaging/lib/python3.8/site-packages
zip -r function.zip \
    FTPThroughLambda.py \
    "${packageDir}/environs/" \
    "${packageDir}/marshmallow/" \
    "${packageDir}/dotenv/"
```

**AWS IAM Permissions**

Grant `s3:GetObject` and `s3:DeleteObject` permissions to the Lambda execution role. Assign the AWS managed policy `AWSLambdaExecute` for it to be able to publish logs to CloudWatch.

**AWS Lambda function configuration**

Create the trigger on the S3 bucket.

Link your IAM role.

Add the following environment variables:

* `user` - FTP username
* `ip` - FTP hostname or IP
* `password` - FTP account password
* `remoteDirectory` - Path on the FTP server to put the files

Choose the file path to your archive with the function and its dependent modules.

Save your function.

**Attribution**

Uses code forked from <https://github.com/Vibish/FTP_SFTP_LAMBDA> with modification.